# Livestreamer.user.js #

Livestreamer.user.js is a userscript and registry entry to allow "Open in livestreamer" links to show in browser.


# Installing for streamlink#
- Install [streamlink](https://streamlink.github.io/).
- Install the [registry entry](https://bitbucket.org/Abex/livestreamer.user.js/raw/master/streamlink.reg)
- Install the [user script](https://bitbucket.org/Abex/livestreamer.user.js/raw/master/Livestreamer.user.js)
- Setup your `streamlinkrc` to have `default-stream`s


# Installing for livestreamer (not maintained)#
- Install [livestreamer](http://docs.livestreamer.io/).
- Install the [registry entry](https://bitbucket.org/Abex/livestreamer.user.js/raw/master/livestreamer.reg)
- Install the [user script](https://bitbucket.org/Abex/livestreamer.user.js/raw/master/Livestreamer.user.js)
- Setup your `livestreamerrc` to have `default-stream`s

# livestreamerrc #
`livestreamerrc` is a set of defaults for livestreamer. On windows this is located in `%appdata%\livestreamer`. It may not exist by default.

Mine looks like:

	player="C:\Program Files\Combined Community Codec Pack 64bit\MPC\mpc-hc64.exe"
	player-passthrough=hls,http,rtmp
	player-no-close
	default-stream=medium,360p

# Supported Sites #
Currently there are configs for youtube and twitch.tv, however adding more should be trivial aslong as they are supported by livestreamer/streamlink. Create a issue or PR if you want one added.