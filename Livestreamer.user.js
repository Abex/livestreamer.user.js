// ==UserScript==
// @name        Livestreamer
// @namespace   Abex
// @description Opens livestreamer
// @include     https://www.twitch.tv/*
// @include     https://www.youtube.com/watch*
// @version     1
// @grant       none
// ==/UserScript==

var ButtonLoc={
  "twitch.tv":{
    Selector:"dl.cn-tabs:nth-child(2)",
    Class:"button",
    CSS:"height:30px;margin-top:10px;margin-left:10px;",
  },
  "youtube.com":{
    Selector:"#watch-headline-title",
    CSS:"color: #fefefe;background-color: #e62117;border-radius:2px;padding:4px;"
  }
};

function makeButton(){
  var el=document.createElement("a");
  el.innerHTML="Open in Livestreamer";
  el.className="_livestreamer_button";
  el.href="livestreamer:"+window.location.href;
  return el;
}

var addedButton=false;
function tryAddButton(){
  if(addedButton) return;
  var s=document.location.hostname.split(".");
  for(;s.length>1;s=s.slice(1)){
    var loc=ButtonLoc[s.join(".")];
    if(loc){
      if(typeof loc=="function") {
        addedButton=loc();
      }else{
        var el=document.querySelector(loc.Selector);
        if(!el) return;
        var button=makeButton();
        if(loc.Class){
          button.className+=" "+loc.Class;
        }
        el.appendChild(button);
        addedButton=true
        if(loc.CSS){
          var style=document.createElement("style");
          style.innerHTML="._livestreamer_button{"+loc.CSS+"}";
          document.head.appendChild(style); 
        }
      }
      break;
    }
  }
}

window.onload=tryAddButton;
document.addEventListener("domready",tryAddButton);